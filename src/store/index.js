import { createStore } from 'vuex';

import axios from 'axios';

export default createStore({
    state: {
        count: 0,
        color: 'steelblue'
    },
    mutations: {
        increaseCount(state, randomNumber) {
            state.count += randomNumber;
        },
        decreaseCount(state, randomNumber) {
            state.count -= randomNumber;
        },
        setColor(state, newValue) {
            state.color = newValue;
        }
    },
    getters: {
        countSquared(state) {
            return state.count * state.count;
        }
    },
    actions: {
        async increaseCount({ commit }) {
            const response = await axios.get('https://www.random.org/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new');
            commit('increaseCount', response.data);
        },
        async decreaseCount({ commit }) {
            const response = await axios.get('https://www.random.org/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new');
            commit('decreaseCount', response.data);
        }
    }
});
