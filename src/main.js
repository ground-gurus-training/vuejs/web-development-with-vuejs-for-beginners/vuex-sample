import { createApp } from 'vue';
import App from './App.vue';
import store from './store';

import PrimeVue from 'primevue/config';
import Button from 'primevue/button';
import Card from 'primevue/card';
import InputText from 'primevue/inputtext';

const app = createApp(App);

app.use(PrimeVue, { ripple: true });
app.use(store);

app.component('Button', Button);
app.component('Card', Card);
app.component('InputText', InputText);

app.mount('#app');
